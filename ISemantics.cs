namespace nilnul.lang
{
	/// <summary>
	/// In linguistics, syntax (/ˈsɪntæks/ SIN-taks)[1][2] is the study of how words and morphemes combine to form larger units such as phrases and sentences. Central concerns of syntax include word order, grammatical relations, hierarchical sentence structure (constituency),[3] agreement, the nature of crosslinguistic variation, and the relationship between form and meaning (semantics). 
	/// </summary>
	/// 
	/// alias:
	///		etymology
	///		sense
	///
	/// vs:
	///		syntax
	///			,which does not provide any information about the meaning of the program or the results of executing that program.
	///		<see cref="nilnul.lang.tier_._sentence._locution.hickey.IEpisteme"/>, which is the semantic of hickey
	interface ISemantic {
		/*
		 there are a number of methods you can use to describe semantics. Denotational, operational and axiomatic semantics being classes of methods for semantics.
		 */
	}
}
