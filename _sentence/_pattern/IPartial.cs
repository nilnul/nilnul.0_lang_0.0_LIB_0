﻿namespace nilnul.lang.tier_._sentence
{
	/// <summary>
	/// phrase; including: prime phrase
	/// </summary>
	/// alias:
	///		handle
	///			, the leftmost direct idiom (reducible) of a 句型
	interface IPartial { }
}
