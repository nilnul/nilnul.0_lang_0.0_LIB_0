﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_
{
	/// <summary>
	/// some character cannot be used in ling.
	/// eg:
	///		-some characters cannot be used in Chinese.
	/// </summary>
	/// <remarks>
	/// all the characters are called alphabet.
	/// </remarks>
	/// alias:
	///		alphabet
	///		字
	///	vs:
	///		morpheme
	internal class ICharacter
	{
	}
}
