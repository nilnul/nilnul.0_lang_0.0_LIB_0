namespace nilnul.lang.tier_
{
	/// <summary>
	/// eg:
	///		word
	///		number
	///		punctuator
	///		whitespace
	///			, which is not token, but to separate tokens in preprocessing.
	/// </summary>
	/// alias:
	///		symbol
	///		token
	///		sign
	///		note
	///		mark
	///		docket
	///		emblem
	///		badge
	interface ISymbol { }
}
