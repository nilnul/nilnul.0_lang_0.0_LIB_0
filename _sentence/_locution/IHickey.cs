namespace nilnul.lang._sentence._substance
{
	/// <summary>
	/// <see cref="nilnul.txt"/>
	/// </summary>
	/// <remarks>
	/// any basic hickey should not be empty and shall refer to something substantial.
	/// corner or trivia cases can later be included using a term derived from the main hickey.
	/// eg:
	///		triangle shall not include trivia case such as a point, or three points on a line.
	///		word shall not be empty.
	/// </remarks>
	/// alias:
	///		hickey
	///		emblem, which is embedded;
	///		lexeme
	///		keepsake
	///		sake
	///		matter
	///		gadget
	///		gist
	///		widget
	///		gizmo
	///		gimmick
	///		dingus
	///		jigger
	///		tok
	///		token
	///		docket
	///		ticket
	///		tkt
	///		held, as held in <see cref="_sentence.IPattern"/>
	///		
	interface IHickey { }
}
