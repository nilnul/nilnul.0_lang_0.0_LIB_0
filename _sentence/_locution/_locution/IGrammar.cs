﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.character.token.expr
{
	/// <summary>
	/// how a locution is formed from:
	///		locution,
	///		or token|hickey
	///	eg:
	///		偏正词组
	/// </summary>
	/// <remarks>
	/// due to the pairing of parenthesis, this is beyond the scope of <see cref="nilnul.txt"/>; <see cref="nilnul.lang_.calc"/>
	///
	/// </remarks>
	/// alias:
	///		lexicology
	internal class IGrammar
	{
	}
}
