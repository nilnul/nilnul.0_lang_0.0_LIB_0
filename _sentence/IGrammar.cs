﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence
{
	/// <summary>
	/// syntax, A subdivision of grammar that deals with how words are put together to form phrases, clauses, and sentences. Syntax is solely involved with descriptive grammar.
	/// </summary>
	/// 
	/// alias:
	///		句法
	///		syntax
	///	vs:	<seealso cref="_tier.IGrammar"/>
	///		Grammar is the set of rules that govern a language, while syntax is a subset of grammar that governs word order and sentence structure

	internal class IGrammar
	{
	}
}
