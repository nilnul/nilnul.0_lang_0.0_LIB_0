﻿namespace nilnul.lang.tier_._sentence
{
	/// <summary>
	/// a nary relation which is used to fill one hole in <see cref="IPattern"/>, which is multinary relation.
	/// </summary>
	/// alias:
	///		jargon
	///		entity
	///		vocab
	///		argot
	///		lingo
	///		jive
	///		filling
	///		filler
	///		monolith
	///		independent
	///		constant
	///		as is
	///		Substance
	///			,Traditionally understood as an entity that doesn't depend on another entity to exist.
	///			,substitute into the blank
	///			, the stance to fill
	///		item
	///		existence
	///		esse
	///		element
	///		marrow
	///		pith
	///		core
	///		esse
	///		locution
	///		
	interface ISubstance { }
}
