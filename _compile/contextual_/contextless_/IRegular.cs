﻿namespace nilnul.lang.grammar_.contextual_.contextless_
{
	/// <summary>
	/// the left hand side is again only a single nonterminal symbol,
	/// The right side may be the empty string, or a single terminal symbol, or a single terminal symbol followed by a nonterminal symbol, but nothing else.
	/// </summary>
	/// (Sometimes a broader definition is used: one can allow longer strings of terminals or single nonterminals without anything else, making languages easier to denote while still defining the same class of languages.)
	interface IRegular {

	}
}
