﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._lang._syntax.grammar_
{
	/// <summary>
	/// lexcicography;
	/// </summary>
	/// <remarks>
	/// </remarks>
	/// keywords:
	///		glossary, lexicograph, lexer
	/// 
	/// alias:
	///		lex
	///			,'l' means linear; "ex" can mean expression in RegEx
	/// vs:
	///		tex, where "t" means tree like, whileas "l" in lex means linear;
	/// <see cref="nilnul.data.ILex"/>
	internal class ILex
	{
	}

}
