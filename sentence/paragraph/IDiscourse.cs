﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence.paragraph
{
	/// <summary>
	/// eg:
	///		xml document
	///		css stylesheet
	///		c# type
	/// </summary>
	/// <remarks>
	/// the content of html "article"
	/// </remarks>
	/// alias:
	///		article
	///		memo
	///		disquisition
	///		dissertation
	///			dissolution
	///			dissection
	///		elaborate
	///		tractate
	///		essay
	///		prose
	///		
	internal class IDiscourse
	{
	}
}
