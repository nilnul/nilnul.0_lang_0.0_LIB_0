﻿namespace nilnul.lang.tier_
{
	/// called  collectionOfBooks in ling; we avoid the term:library here to reduce confusion.
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// comprises many <see cref="IAssembly"/>;
	/// </remarks>
	/// alias:
	///		sln
	///		references
	///		package
	///		deploy
	///		appDomain
	///		program
	///		pac
	///		deployable
	///		corpus
	///		corpora
	///		
	///	note:
	///		an application may start multiple Ui, processes, or threads.
	interface IApplication { }

}
