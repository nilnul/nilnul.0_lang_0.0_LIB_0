﻿namespace nilnul.lang.tier_
{
	/// <summary>
	/// all source code.
	/// </summary>
	/// <remarks>
	/// comprises many <see cref="IApplication"/>
	/// </remarks>
	/// alias:
	///		codebase
	///		source
	///		corpora|corpus
	///		resource
	///		kit
	///		src
	///	vs:
	///		lang, bigger than this.
	///		
	interface ICodebase { }

}
