﻿namespace nilnul.lang.tier_.sentence.paragraph.section.chapter.book
{
	/// <summary>
	/// many corpus is called corpora
	/// that is the <see cref="nilnul.ILang"/>
	/// </summary>
	/// alias:
	///		corpuses
	///		corpuss
	interface ICorpora { }
}
