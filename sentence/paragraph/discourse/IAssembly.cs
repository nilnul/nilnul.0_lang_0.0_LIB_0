﻿namespace nilnul.lang.tier_.sentence.paragraph.discourse
{
	/// <summary>
	/// for computer lang, this is like .dll;
	/// </summary>
	/// <remarks>
	/// comprises many <see cref="IType"/>
	/// </remarks>
	/// for ling, this is
	///		a single book?
	///		, or
	///		a collection of books
	/// alias:
	///		ledge
	///		base
	///		codebase
	///		reservoir
	///		book, of many chapter, each of which is a <see cref="IDiscourse"/>
	///		repo
	///		depo
	///		project
	///		program
	///		assembly
	///		library
	///	vs:
	///		repo, like git repo.
	///			,for which when human knowledge of mastering more complexity grows, it will grow;
	///			,currently we for each assembly create a repo. Or we may create a repo for multiple assemblies including library and test, as many others do;
	///	vs:
	///		prose, which is the key unit for human communication; but for programming tool such as VisualStudio, the key unit is bigger, and it is project
	interface IAssembly { }

}
