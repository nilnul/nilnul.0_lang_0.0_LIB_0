﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence.paragraph.discourse
{
	/// <summary>
	/// take away from an article. not a summary, as this may ignore some parts of the article, such as those are already known, or insignificant that do not need attention or care.
	/// </summary>
	/// vs:
	///		<see cref="_disourse.IAbstract"/>, which is part of the discourse, and is often put under the topic, whileas this is an extension of the article, often put after the article.
	internal class IExtract
	{
	}
}
