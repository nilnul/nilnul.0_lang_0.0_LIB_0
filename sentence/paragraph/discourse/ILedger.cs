﻿namespace nilnul.lang.tier_.sentence.paragraph.discourse
{
	/// <summary>
	/// 
	/// </summary>
	/// alias:
	///		book, if the discourse is chapeterable article;
	///		catalog		, as called in db, if the discourse is table of data;
	///		brochure
	///		ledger
	///		pamphlet
	///		manual
	///		booklet
	///		gazette
	///		zine, as in magazine
	///		
	interface ILedger { }

}
