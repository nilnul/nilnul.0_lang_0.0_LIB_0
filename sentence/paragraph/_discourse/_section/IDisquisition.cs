﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence.paragraph._disourse._section
{
	/// <summary>
	/// the content of a section, without the headline;
	/// </summary>
	/// this itself is a <see cref="IDiscourse"/>
	/// alias:
	///		disquisition
	///			,like composite
	///		disquisite
	///		deliberate
	///		elaborate
	///		
	///		
	/// 
	internal class IDisquisition
	{
	}
}
