﻿namespace nilnul.lang.tier_.sentence.paragraph.discourse_
{
	/// <summary>
	/// title and paragraphs;
	/// </summary>
	/// <remarks>
	/// like a evt in google calendar that is intended as a note, comsisted of by title and paragraphs.
	/// </remarks>
	/// alias:
	///		notice
	///			,a title and the disquisition
	///		note
	///			,as done in notepad
	///		msg
	///		alert
	///		alarm
	///		reminder
	///		info
	///		
	interface INotice { }
}
