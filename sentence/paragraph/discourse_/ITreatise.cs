﻿namespace nilnul.lang.tier_.sentence.paragraph.discourse_
{
	/// <summary>
	/// with sections. 
	/// </summary>
	/// <remarks>
	/// like the capstone, thesis, dissertation
	/// </remarks>
	/// alias:
	///		tractate
	interface ITreatise { }
}
