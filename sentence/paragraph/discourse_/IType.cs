﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_
{
	/// <summary>
	/// procedures with state, enclosed together to expose API to read state or to transit state.
	/// eg:
	///		class, including fields, properties and methods .
	/// </summary>
	/// <remarks>
	/// comprises many <see cref="IFunction"/>, where data is a trivia|constant function.
	/// </remarks>
	/// in ling, this is called a chapter|essay (or a book?). this is at same scale|level as xml document ,js script snippet, sql script, html document, css stylesheet, xlsx sheet (not book, or ledger). So it might be bettered mapped to essay|composition|disquisition.
	///		
	/// alias:
	///		script
	///		essay
	///		doc
	///		chapter
	///		transcript
	///		manuscript
	internal class IType
	{
	}
}
