﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence.paragraph.discourse_
{
	/// <summary>
	/// no section. title with many paragraphs.
	/// </summary>
	/// <remarks>
	/// like the composition, a homework for junior school students.
	/// </remarks>
	/// alias:
	///		notice
	///		note
	///		prose
	///		msg
	internal class IEssay
	{
	}

}
