﻿namespace nilnul.lang.tier_.sentence.paragraph.procedure.method
{
	/// <summary>
	/// to reuse a method, one can
	///		: call
	///		: inline the call in compiled code
	/// </summary>
	/// vs:
	///		<see cref="procedure.IReuse"/>
	interface IReuse { }



}
