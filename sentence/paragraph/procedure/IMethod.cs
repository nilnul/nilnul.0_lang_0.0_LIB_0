﻿namespace nilnul.lang.tier_.sentence.paragraph.procedure
{
	/// <summary>
	/// parameterized procedure.
	/// to reuse a procedure, one has to
	///		:copy&paste
	///		:macro or snippet. to reuse a method, we can call
	/// </summary>
	/// this is on the same level as essay, or section in ling
	///
	///  when in declaration, function encloses procedure. when in runtime, procedure can also evoke function. So the two can recursively call each other.
	/// 
	/// alias:
	///		function
	///	vs:
	///		<see cref="IProcedure"/> is executed|runThru|evaluted|NotOnPath, while method is called.
	///

	interface IMethod { }



}
