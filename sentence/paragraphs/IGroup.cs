﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence.paragraphs
{
	/// <summary>
	/// eg:
	///		<div>
	///			<para></para>
	///			<para></para>
	///		</div>
	///		<para>
	///		</para>
	///		<div>
	///			<div>
	///				<para>
	///				</para>
	///				<para>
	///				</para>
	///			</div>
	///			<div>
	///				<para>
	///				</para>
	///				<para>
	///				</para>
	///			</div>
	///			<para>
	///				implicit div
	///			</para>
	///		</div>
	/// </summary>
    internal class IGroup
    {

    }
}
