﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.semantic
{
	/// <summary>
	/// type systems are as often considered the "static semantics" of a language as they are a "syntactic discipline for correctness".
	/// </summary>
	/// <remarks>
	/// including:
	///		type inference, and annotating that;
	///		type checking.
	/// </remarks>
	/// vs:
	///		dynamic behavior
	internal class IStaticStruct
	{
	}
}
