using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.run_
{
    /// <summary>
    /// if a lang is interpreted, it's called Script.
    /// If a lang is compiled before run, it's called Programming;
    /// </summary>
    /// <remarks>
    /// thru JustInTime compiler.
    /// </remarks>
    internal class Interpret
    {
    }
}
