namespace nilnul.lang
{
	/// <summary>
	/// similar to a block of code, but with explicit contextual (such as declaration|induction of new vars, scope of variables ) callsite, which might be a frame of the call stack;
	/// </summary>
	///
	/// <see cref="nilnul.lang.functional"/>
	interface IProcedure { }
}
