using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.sentence_.primitives
{
	/// <summary>
	/// as each sentence is already ended with ';', we don't need any separator here.
	/// to use separator, <see cref="_sentence."/>
	/// </summary>
	internal class IToTxt
	{
	}
}
