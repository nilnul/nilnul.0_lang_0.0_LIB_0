﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._lang
{
	/// <summary>
	/// lang, by syntax, is a collection of tex.
	/// each tex is a string of chars, following a grammar.
	/// </summary>
	/// alias:
	///		tex
	///			,Tree expr?
	/// vs:
	///		txt, a string of chars. no grammar is involved, or only simple rule such as regex is used;
	internal class ITex
	{
	}
}
