﻿namespace nilnul._lang._sys
{
	/// <summary>
	/// represented by
	///		(state, state)
	///		, the transition from one state to another.
	///	we need more than <see cref="_lang.ILex"/>, including: more complex <see cref="nilnul.lang.ITex"/> (grammar). lex+grammar=syntax.
	/// </summary>
	/// alias:
	///		sentencial
	///		sentenle
	interface ISentence { }


}
