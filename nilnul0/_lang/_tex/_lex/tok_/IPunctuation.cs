﻿namespace nilnul._lang.tok_
{
	/// <summary>
	/// eg:
	///		,
	///		.
	///	note:
	///		' ' the space is not tok; thus it's not punc.
	/// </summary>
	/// alias:
	///		punc
	///	vs:
	///		junc, <see cref="nilnul.txt_.vered_._id._nom"/>
	interface IPunctuation { }
}
