﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._lang
{
	/// <summary>
	/// a system
	///		,statically structure|state, when the system stops (before, pause, or end; or during run)
	///		, and dynamically behavior|changeOfState, when the system runs
	/// </summary>
	/// <remarks>
	/// for an app, sys=db+app
	/// for a lang procedure or function body, sys=data+sentence
	/// 
	/// for a class, sys=field+method
	/// </remarks>
	internal class ISys
	{
	}
}
