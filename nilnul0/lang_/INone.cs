﻿namespace nilnul.lang_
{
	/// <summary>
	/// no txt is valid;
	/// this is the sublang of all other langs; every language shall allow empty sentence;
	/// </summary>
	/// <remarks>
	/// eg:
	///		"" is not valid json, that means json in this regard is improvable.
	/// </remarks>
	public interface INone { }

}
