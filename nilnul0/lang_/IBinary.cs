﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang_
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	///这个最经典的例子就是计算机出生的人设计的协议一般都是ascii，都是human readable的。通讯出生的人设计的协议都是binary的，都是以bit为基本单位。比如h.323和SIP；比如通讯领域喜欢用asn.1，而cs领域喜欢用xml。
	///学通讯的人在设计协议的时候会通盘考虑传输效率，会事无巨细的约定所有细节规范，而计算机出生的人设计协议的时候考虑的是协议本身是否简单易懂，是否方便调试，并且会倾向于把协议设计的足够抽象，而不是去事无巨细的罗列一堆细节。至于传输效率的问题，那是后面叠加一个压缩算法的事，不需要在协议层面考虑。
	/// </remarks>
	/// lvsoft (Lv(The Last Guardian)), 信区: Circuit
	/// 标  题: Re: 大家遇到嘉立创smt贴错阻容么...发信站: 水木社区 (Fri Dec 22 12:23:47 2023)
	internal class IBinary
	{
	}
}
