namespace nilnul.lang_
{
	/// <summary>
	/// vs:<see cref="IScript"/>, this is precomiled for each Os, and it's the compiled code that is run.
	/// </summary>
	/// <see cref="nilnul.lang.run_.ICompile"/>
	interface IProgram:nilnul.ILang { }

}
