using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.tier_.sentence_
{
	/// <summary>
	/// keywords:ab,cd,efg;
	/// ,where ',' is used rather than ';', which is reserved for ending the sentence to allow for subsequent sentences;
	/// </summary>
	/// <remarks>
	/// <see cref="nilnul.obj.co._PhraseX.COMMA4SEPARATOR"/> for why ',' is used between keyword
	/// </remarks>
	/// alias:
	///		keywords
	///			,key with words
	internal class IKeywords
	{
	}
}
