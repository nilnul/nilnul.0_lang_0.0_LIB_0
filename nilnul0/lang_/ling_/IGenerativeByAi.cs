using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang_.ling_
{

	/// <summary>
	/// Human ling is used mainly for communication, not for thinking, even ling can represent the thinking process or result. The thinking machinery might be not fully embodied by generative AI.
	/// </summary>
	/// <remarks>
	/// but the thinking represented by generative Ai can be reorganized by Ai to reinvigorate|repeat the thinking process.
	/// </remarks>
	internal class IGenerativeByAi
	{
	}
}
