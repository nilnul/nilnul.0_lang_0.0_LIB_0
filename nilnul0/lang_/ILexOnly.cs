﻿namespace nilnul.lang_
{
	/// <summary>
	/// lex only, no <see cref="nilnul.lang.ITex"/>;
	/// </summary>
	/// <remarks>
	/// as there is no tex, this can be processed by <see cref="nilnul.txt"/>
	/// </remarks>
	interface ILexOnly { }

}
