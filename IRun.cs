﻿namespace nilnul.lang
{

	/// <see cref="ILang"/> is linear; to represent nonlinear structure, we need special parsing rules to parse some component for reconstructing a structure; <see cref="nilnul._lang.syntax.IBuild"/>
	/// <summary>
	/// after compiled, the lang becomes program to be <see cref="_run.ILinker"/> and <see cref="_run.ILoader"/>
	/// </summary>
	public interface IRun { }
}
