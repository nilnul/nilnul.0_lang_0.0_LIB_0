﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.syntaxs.be_.equiv_
{
	/// <summary>
	/// for a same semantic meaning, we can use many syntaxes:
	///		1) tab or space or line break?
	///		2) one whitespace or more?
	///		3) block singleton or a primitive sentence?
	///		4) return early or single exit|break for a function?
	///		5) parenthesis to make clearer the precedence of operators?
	/// </summary>
	/// <remarks>
	/// this is called coding style.
	/// </remarks>
	internal class ISemantically
	{
	}
}
