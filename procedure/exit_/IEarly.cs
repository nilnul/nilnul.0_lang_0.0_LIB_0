using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.procedure.exit_
{
	/// <summary>
	/// exception would break early;
	/// besides,
	///		you can use "goto" in a procedure or block of code, "return" in a function, and "break" in a loop to exit early.
	///		
	/// </summary>
	/// <remarks>
	///
	/// </remarks>
	internal class IEarly
	{
	}
}
