﻿namespace nilnul.lang
{
	/// <summary>
	/// in a lang, we have many construct|composition.
	/// Some of the construct is made of smaller scale subconstruct.
	/// eg:
	///		an essay is made of sections, each of which is made of paragraphs; paragraph is made of sentences; sentence is made of clauses; clause, as a pattern, is made of expressions and templates(or multinary relations); expression is made of subexpression and operators; and subexpression is made of words; word is made of letters.
	///		we can also construct towards bigger scale: essays can act as chapters to make up book. books can form a library, simillar to a computer program library.
	/// </summary>
	/// <remarks>
	/// by recursively assembling, we flatten the learning curve. eg:
	///		child learns letters, then words, then pattern|association(relation), then clause, sentence, paragraph|blockOfCode and hence paragraphs, memo0essay(without sections)|section|discourse|disquisition|essay, these are for pre-college students.
	///		and later more advanced:section|discourse(memo with recursive sections)|disquisition like treatise. script.
	///		and then for professional acadmia,   book (with many treatises as chapters), books like serious, correlated shelf/group/suite|cluster. program
	///		and more broad for the society: library|package|repo|reservoice|academia|ken. But this is just simple <see cref="nilnul.IObjs"/> of book. no relation about the books, as the relation will be embodied not here ,but back into new books  such as indexes in library, or information|resourseManagement|libraryMgt|literatureResearch|bibliography0referencesResearch major|discipline, corpus, corpora.
	///		and then all the lang
	/// </remarks>
	///
	/// 
	/// alias:
	///		tier,like tiles for each layer 
	///		hier
	///			, with many tiers
	///		model
	///		construct
	///		phase
	///		level
	///		tier
	///		make
	///		assembly
	///		structure
	///		component
	///
	interface ITier { }
}
