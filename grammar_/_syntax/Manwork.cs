﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._lang._syntax
{
	/// <summary>
	/// var a="abc".length+"def".length;
	/// or
	/// const a=6;
	/// or you can directly use 6 to save the storage space for 6.
	///
	/// That boils down whether you would:
	///		1) let the compiler and runtime to do the work
	///		2) you do the work and save some runtime space or duration.
	///	
	/// </summary>
	/// <remarks>
	/// If we do more manwork, the computer can cost less runtime and less space. maybe that's how the collaboration of human/computer works and where the meaningness of human is to computer;
	/// we human kind can also develop some theorems so that we can apply that in our algorithms to shorten the time a bruteforce algorithms would take;
	/// </remarks>
	internal class Manwork
	{
	}
}
