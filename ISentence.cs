namespace nilnul.lang
{
	/// <summary>
	/// while lex represents data, sentence represences the behavior/movement which changes/transits data|state
	/// </summary>
	/// <remarks></remarks>
	/// for more complex concept than data and behavior, <see cref="tier_.sentence.paragraph.IDiscourse"/>
	interface ISentence { }
}
