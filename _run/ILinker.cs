﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang._run
{
	/// <summary>
	/// 链接器是编程语言或操作系统提供的工具，它的工作即是解析未定义分符号，通过将目标文件中的占位符替换为符号地址;
	/// 链接是把目标文件、操作系统的启动代码和用到的库文件进行组织形成最终生成可加载、可执行代码的过程;
	/// </summary>
	/// <remarks>
	/// link libraries into the main execution program;
	/// </remarks>
	/// <see cref="nilnul._lang.syntax._build.Phase_.Link"/>
	internal class ILinker
	{
	}


}
