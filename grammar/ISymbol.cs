﻿namespace nilnul.lang
{
	/// <summary>
	/// a symbol is the left hand side of grammar rules;
	/// </summary>
	/// <remarks>
	/// for <see cref="nilnul.txt.ILex"/>, symbol is called <see cref="nilnul.txt._lex.ITok"/>
	/// </remarks>
	interface ISymbol { }
}
