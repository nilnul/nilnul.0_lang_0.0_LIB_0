﻿using nilnul.order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.syntax.semantic_
{

	/// <summary>
	/// </summary>
	/// <remarks>
	/// "John is a married bachelor." is grammatically well formed but expresses a meaning that cannot be true.
	/// </remarks>
	/// alias:
	///		nonsense
	///		contradiction
	///		paradox
	///		falsehood
	internal class IImposiible
	{
	}
}
