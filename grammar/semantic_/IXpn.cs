﻿using nilnul.order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.syntax.semantic_
{

	/// <summary>
	/// In natural languages, a sentence can be syntactically correct but semantically meaningless. For example:
	/// The man bought the infinity from the store.
	/// The sentence is grammatically correct but doesn't make real-world sense.
	/// </summary>
	/// <remarks>
	/// He drinks rice (wrong semantic- meaningless, right syntax- grammar)
	/// "Colorless green ideas sleep furiously." is grammatically well formed but has no generally accepted meaning.
	/// "John is a married bachelor." is grammatically well formed but expresses a meaning that cannot be true.
	/// </remarks>
	/// alias:
	///		nonsense
	internal class IXpn
	{
	}
}
