using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.app_
{
	/// <summary>
	/// language is primarily a tool for communication rather than thought; -by Evelina Fedorenko, on Nature;
	/// </summary>
	/// <remarks>
	/// language is not a premise for thought; it's not necessary nor sufficient for thinking;
	///( This is contrary to the mainstream opion that thinking is "using language".)
	///
	/// and language is influenced by the pressure of communication.
	/// </remarks>
	internal class IComm
	{
	}
}
